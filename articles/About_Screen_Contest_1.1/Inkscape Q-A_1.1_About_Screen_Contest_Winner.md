# Meet Ozant Liuky, Winner of the Inkscape 1.1 About Screen Contest

We recently caught up with Ozant, to ask him some questions about himself, and his use of Inkscape.

Inkscape: Please introduce yourself to the Inkscape Community;
What is your name, Where in the World do you live?

ozant liuky: Hi, My name is Fauzan Syukri, but in the digital world my nickname is ozant liuky. I live in the coastal region of West Sumatra in Pariaman, Indonesia. It's a small city, but has a nice beach area and delicious food. On a normal day, the city looks quiet, but on Eid days (https://en.wikipedia.org/wiki/Eid_al-Fitr) or the Tabuik festival (https://en.wikipedia.org/wiki/Tabuik) the city will be crowded with visitors or residents who return to their hometowns.  

I: When did you first discover Inkscape? Please explain how you came to use Inkscape.

ol: Before I got to know Inkscape, I used Macromedia Flash software. In college I was very happy to use it, because in addition to creating animation, Macromedia Flash software can also be used for vector drawing. In the past, I created presentations for my final project at university and work presentations using Macromedia Flash.

I have also made short films and uploaded them to babaflash.com, a local site that contains animated works made in the flash format. Unfortunately, the site is no longer there and my work is also gone and I don't have a copy.

A little while later, around 2012, I started laerning Inkscape and I started choosing legal software to use. Before that I didn't really understand the meaning of cracked or pirated software. I tried Inkscape and I was familiar with how it worked, somewhat similar to Macromedia Flash, without the animation feature.

I started learning it, at first I had a little trouble using it, I started looking for online tutorials, on various sites, and started trying to put the things I had learned into practice. I grew more and more when I found the tutorial site from Chris Hildenbrand. That's when I started to understand Inkscape very well and everything started to feel easy to learn.

Besides that, the Inkscape Facebook group has also really helped me grow. It's a good and friendly community. Apart from Facebook, I also followed the Inkscape group on Google Plus before it stopped. And there you have it! Since then I have continued to use Inkscape until recently.

I: Do you use Inkscape exclusively, or do you use other software in your work flow?

ol: Almost yes, but once I used blender to help me draw the right perspective for a 3D object. I've always thought about combining Inkscape and Blender skills. I also studied Blender in the hope that my skills could support my activities in Inkscape, such as helping me make perspective drawings from difficult angles, or create animation.

I: What motivated you to take part in the contest?

ol: First, I wanted to show the new or the old user what can they achieved with Inkscape. I tried to make my best artwork using only Inkscape. It has the ability to create artwork that can compete with commercial software. What you need besides a good tool is good knowledge on to use it, how to work effectively and knowledge in the field of design or knowledge in drawing or painting.

Secondly, I wanted to prove to myself that I have grown over the past years. I participated in the Inkscape 0.92 About Screen Contest. This time I decided to try again. I wanted to see how far I could get. I really tried to design an image worthy of this contest. I started scribbling in a notebook for a few days and looked for various references. Then I was in no rush to finish. I really enjoyed the process. For several days I looked at every step of the drawing I had made. I tried to add new ideas to it, until finally I finished it. The result is what you see today.

I: How did you come up with the idea for your winning design in this years "1.1 About Screen Contest"?

ol: I love robots and I like drawing. I also like drawing landscapes. So, I put all the elements into my drawing -  a robotic hand painting a flower. The robot is connected to the computer and Inkscape. At first I made a rocket as a model being painted, but I replaced it with a flower, to include natural elements in my drawing. 

I: Which feature or bugfix of the upcoming version are you looking forward to the most? https://inkscape.org/release/inkscape-1.1/?latest=1

ol: I like the cut-and-paste parts of the paths feature with the Node tool. I think it is a good feature that helps us cut paths with a particular shape. The other feature I like is exporting as PNG, because it saves time.

I: If you could wave a magic wand, what one thing would you like to see in Inkscape?

ol: One Thing I want to see in inkscape is an animation feature. I love to see my drawing in motion. And I want do it inside Inkscape only.

I: What would be your best advice to anyone wanting to use Inkscape in a creative way?

ol: Learn the basics you need most with your workflow. Enjoy the workflow that comes with Inkscape. Sometimes it may not be the same as for other software that you have used. Inkscape is unique in its own right.

I: Lastly, where can people find you and your artwork online?

ol: You can see my artwork on https://dribbble.com/ozant and my instagram https://www.instagram.com/ozantliuky/


Thank you Ozant, for answering our questions, and for being a fantastic member of the Inkscape Community.

To the rest of the Community: Next time, it could be YOU, answering questions like these.

Remember, whenever you draw... Draw Freely. 
