GSOC Recruitment article - for early March - once Inkscape has been accepted and announcement made by Google (scheduled for March 9, 2021) [How it works | Google Summer of Code](https://summerofcode.withgoogle.com/how-it-works/#timeline)



# Join Inkscape's GSoC 2021 Team! GSoC 2020 students contributed custom features, modern code & updated documentation to Inkscape 



We are happy to be part of the [Google Summer of Code](https://summerofcode.withgoogle.com/) (GSOoC) 2021 program to enable students to work with mentors to help advance our free and open source (OS) project and make a difference for our users.  

When the Inkscape community seeks GSoC applicants, we're hoping to find people who are curious about the project and the software. Part of the application process requires applicants to complete sample work on the project to demonstrate their skills and understanding. Inkscape core developers Tavmjong Bah (@Tav), and  Thomas Holder (@speleo3), Marc Jeanmougin (@Mc) and Krzysztof Kosinski mentored our students during their time with the project.

Warning: you might just come and get involved in our online community! 

**Inkscape GSoC 2020 alumni**
In 2020, our three GSoC students, Valentin,  Rathod Sahaab and Moazin worked on different projects, from developing custom features and modernizing code to studying and updating older documentation.



**Developing a custom-built dialog docking system**   

Valentin Ionita ([@vanntile](https://gitlab.com/vanntile)), from Bucharest, Romania, joined the Inkscape community in winter 2019. He has been a GSoC student for two years in a row. In 2019, he worked on mesh gradients and hatches to enable non-technical users of Inkscape to more easily use them. 

In 2020, Valentin, "driven by passion and a sense of community," turned his talents to Inkscape once more; he proposed working on "removing the Gnome Docking Library (GDL) in favor of a custom-built, extensible dialog system, based on GIMP's own multi-panes and demos from (Inkscape developer) Tav." The result is shiny new notebooks (with tabs) in lieu of the old docks! Through this process, Valentin worked on reducing bugs and modernizing code, objectives that will help "both the application quality and the post 1.0 development process." His work will be integrated into Inkscape's 1.1 release in Spring 2021. Find out more about Valentin, including is project proposal, here. (https://gitlab.com/vanntile/inkscape-gsoc-2020 )



**Implementing a Command Palette feature**

Abhay Raj Singh Rathod (@rathod-sahaab), from Hamirpur, India, proposed implementing a Command Palette feature within Inkscape to enable users to search for actions, such as rotating an object, and execute them, and finding recent files. "The command palette remembers the last commands you performed," he explained; this new feature is part of the Inkscape 1.1 alpha release. Jabier Arraiza, Thomas Holder and Patrick Storz mentored Abhay, who continues to be part of the Inkscape community. Read about his work here. (https://dev.to/rathod_sahaab/inkscape-command-palette-merged-15c2). 

*(potential image - 2. Select an object, type rotate, and then press enter...)* 



**Preserving and updating project knowledge on Livarot documentation **

Moazin Khatti (@moazin), from Hyderabad District, Pakistan, proposed "reading, understanding, and redesigning the library 'Livarot' (used in Inkscape for path simplification, path offsetting, boolean operations, and tweaking) that has remained a black box for Inkscape developers for the past 14 years." He has continued to be part of the community, thanks to his well-earned expertise with one of the foundational libraries in Inkscape. With support from his mentors, Inkscape developers Thomas Holder, Marc Jeanmougin and Krzysztof Kosiński,  Moazin delivered "comprehensive and well-illustrated documentation to preserve the knowledge gained about the inner working of Livarot." Read about his project, including his proposal here. (https://gitlab.com/moazin/inkscape-gsoc-2020-proposal/-/blob/master/gsoc-final-work.md) 



Do you have an idea for improving Inkscape? Or a burning question, perhaps? If so, get in touch! Let us know what you'd like to work on together.

Remember that to apply, you will need to have contributed to the Inkscape project.

### How to Apply

Here are the steps you need to take before applying:

1. [Read the Wiki page.](https://wiki.inkscape.org/wiki/index.php/Google_Summer_of_Code)
2. [Read the GSOC student guide](https://google.github.io/gsocguides/student/).
3. Introduce yourself[ on our developers' mailing list](https://inkscape.org/en/community/mailing-lists/), [#inkscape-devel](https://inkscape.org/community/discussion/) on IRC or [in our rocket.chat development channel](https://chat.inkscape.org/channel/team_devel).
4. Contribute two patches (small code contributions) to the project. Find out more about [how to do that here](https://inkscape.org/contribute/)!

As you are working on your patches, think about the code project you wish to propose to improve Inkscape.

Once you have introduced yourself, discuss your plans with the development team as you will be collaborating on the GSOC application. The wiki link above outlines the Inkscape project, potential GSOC project areas and mentors.

Inkscapers will be happy to answer your questions about how to get involved.

The Student Application Period runs from **March 29, 2021 to April 13, 2021**. For more details, [visit the GSOC home page](https://summerofcode.withgoogle.com/). Then head to the [Inkscape page on GSOC](https://summerofcode.withgoogle.com/archive/2020/organizations/4527035860385792/) for the specific questions and details.

The deadline for applying to the Google Summer of Code is **April 13, 2021**.

We look forward to meeting you soon!
