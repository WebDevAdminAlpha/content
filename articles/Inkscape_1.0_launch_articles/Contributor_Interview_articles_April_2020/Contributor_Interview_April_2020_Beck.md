Inkscape 1.0 release - April 2020 interview

*Informations sur la soumission*

*Formulaire : [Inkscape Contributor Interview](https://framaforms.org/inkscape-contributor-interview-1587833730)*

*Soumis par Anonyme (non vérifié)*

*sam, 25/04/2020 - 18:01*

*0.0.0.0*



**Contributor Interview: Beck**



Headline

**"I use Inkscape almost daily for professional and personal projects in some form or fashion"**



**Please introduce yourself; what is your name and where in the world do you live?**

Beck, San Antonio, Texas, USA



**When did you first meet the Inkscape Community? Please explain how you came to join the project.**

it has been a few years ago I guess. I followed the group on IRC for years and then moved to the chat.inkscape.org room to chat.



**What are the areas and/or teams of the project that you contribute to or have contributed to?**

I'm not a programmer or developer. I have only reported a bug here or there. I mainly try to help answer end user questions on how to use Inkscape and more of technical functionality.



**What motivates you to contribute to the Inkscape project? What do you like about the community?**

I'm a big Open Source Advocate. So I moved my workflow over to 100% Free and Open Source Software. I use Inkscape almost daily for professional and personal projects in some form or fashion. I love the global community behind Inkscape and Open Source Software



**Please name and explain a few things that you worked on for Inkscape 1.0. Why did you choose to work on those ones?**

I'm not a developer or programmer, so I've only helped test and report things here and there, mostly on Facebook. The Facebook communities around Inkscape have grown a great deal in the past year alone.



**What's your favorite part of the 1.0 release?**

The path effect tools! Being able to quickly change and modify bevel/chamfer and such is great. Also the whole line height spacing issue for font seems to be working as expected now.



**If you could wave a magic wand, what one thing would you like to see in the Inkscape project?**

I think most would say CMYK support. I don't do a lot of printing so I don't know if I'd say that. I might say having something like Snozi or a Prezi style editor that could work with Inkscape and export like Slide.js or Animate CSS export would be fun.



**What would be your best advice to anyone wanting to contribute to the Inkscape project?**

I don't know all the ends and outs of how the projects/code works with GitLab. So it would be nice for some of the head developers to show some video or have a bug fixing day that is live streamed for newbies to watch the process from start to finish to help them understand how it is done.



**Is there anything else you'd like to tell us?**

The whole development team has done a great job and it is so nice to have a solid Open Source Vector editing package that I can train others on.



________________

**We'd like to tag you on social media if we use quotes from your interview. If you are on any of the following platforms, please add your handle below:**

Mastodon:
Twitter: designbybeck
Facebook: designbybeck
Instagram:

How can we contact you for details / clarifications?designbybeck