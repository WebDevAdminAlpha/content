Inkscape 1.0 release - April 2020 interview

*Informations sur la soumission*

*Formulaire : [Inkscape Contributor Interview](https://framaforms.org/inkscape-contributor-interview-1587833730)*

*Soumis par Anonyme (non vérifié)*

*dim, 26/04/2020 - 11:17*

*0.0.0.0*



**Contributor Interview: Shlomi Fish / Rindolf**



Headline 

**Contributed to 1.0 "extensions and the cmake-based build system"**



**Please introduce yourself; what is your name and where in the world do you live?**

My name is Shlomi Fish (שלומי פיש in Hebrew letters), also known as "Rindolf" on IRC and elsewhere, and I live in Tel Aviv, Israel.



**When did you first meet the Inkscape Community? Please explain how you came to join the project.**

Having used Corel-Draw in the past, I became interested first in Sodipodi and later in its Inkscape fork. I started contributing more seriously some years later.



**What are the areas and/or teams of the project that you contribute to or have contributed to?**

I contributed to the core C++ code, to the Python extensions, tried helping users on freenode, and did some QA work.



**Please name and explain a few things that you worked on for Inkscape 1.0. Why did you choose to work on those ones?**

The extensions and the cmake-based build system.



**What would be your best advice to anyone wanting to contribute to the Inkscape project?**

Perhaps read my essay here: https://www.shlomifish.org/philosophy/computers/open-source/how-to-start-contributing/tos-document.html .



**Is there anything else you'd like to tell us?**

Stay cool!



_____________

**We'd like to tag you on social media if we use quotes from your interview. If you are on any of the following platforms, please add your handle below:**

Mastodon:
Twitter: https://twitter.com/shlomif
Facebook: https://www.facebook.com/shlomi.fish/
Instagram:

How can we contact you for details / clarifications?shlomif@shlomifish.org