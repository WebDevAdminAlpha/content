Inkscape 1.0 release - April 2020 interview 

*Formulaire : [Inkscape Contributor Interview](https://framaforms.org/inkscape-contributor-interview-1587833730)*

*Soumis par Anonyme (non vérifié)*

*sam, 25/04/2020 - 16:41*

*0.0.0.0*

**Contributor Interview: anonym_i_ous**





HEADLINE

**From discovering a welcoming, open source community to wrangling bugs & documents**



**Please introduce yourself; what is your name and where in the world do you live?**

My name is Yash Pal Goyal (19y). I live in South Asia.



**When did you first meet the Inkscape Community? Please explain how you came to join the project.**

If I recall correct, I met the Inkscape community around August of last year (2019), when I encountered a problem while making the switch from PowerPoint to Inkscape. I posted it on [inkscape forums](https://inkscape.org/forums/other/copying-shape-from-powerpoint/), and the team guided me to post it as an issue. And that's how my journey in the inkscape project began.



**What are the areas and/or teams of the project that you contribute to or have contributed to?**

I don't have many contributions per se :sweat_smile:.​ I just post the issues (can be filtered by [Author=yashpalgoyal1304](https://gitlab.com/groups/inkscape/-/issues?scope=all&utf8=%E2%9C%93&state=all&author_username=yashpalgoyal1304) and my input on existing issues.

Still, if I were to categorize what I've done, then I'd say feature requests, bug wrangling, sharing the relevant references regarding features, and documentation are the areas that I had (and still have) an interest in.



**What motivates you to contribute to the Inkscape project? What do you like about the community?**

The openness of the community, and as someone put it some time ago, 'No snark bombers', lol. The community is really welcoming and guiding. That is the thing that keeps me coming back. The other thing about the community that motivates me to contribute to the Inkscape project is it being an Open Source Project. 

Also it would not be right if I didn't give credit for the very cool issue tracker Gitlab combined with the idea of 'Friendly Feedback "inbox" ' - a combined place for bug reports and feature requests. All these things are the supporting pillars for my wanting to contribute.



**Please name and explain a few things that you worked on for Inkscape 1.0. Why did you choose to work on those ones?**

As I already have said, I don't have many independent contributions per se, only adding input on issues.



**What's your favorite part of the 1.0 release?**

1) Live Path Effects, 2) Custom Icons (each icon as separate file), and 3) the ability to import .EMF formats (even though as of now, it is partial) and probably there would have been many many MANY more if i had much (or say any) experience with earlier versions.



**If you could wave a magic wand, what one thing would you like to see in the Inkscape project?**

It would be to have the interface based on something like the (hypothetical) Blender Tool kit. (It is just a wish, and I haven't weighed the pros and cons yet.)



**What would be your best advice to anyone wanting to contribute to the Inkscape project?**

Nothing specific, just normal nice human being things ;) - Be humble, and honor the opinions of others, too. Find polite ways to disagree.





--------

CONTACT TO CONFIRM DETAILS AND TO REVIEW FINAL ARTICLE: 

https://chat.inkscape.org/direct/anonym_i_ous

