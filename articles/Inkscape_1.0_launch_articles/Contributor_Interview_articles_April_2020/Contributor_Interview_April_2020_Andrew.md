Inkscape 1.0 release - April 2020 interview

*Informations sur la soumission*

*Formulaire : [Inkscape Contributor Interview](https://framaforms.org/inkscape-contributor-interview-1587833730)*

*Soumis par Anonyme (non vérifié)*

*sam, 25/04/2020 - 21:34*

*0.0.0.0*



**Contributor Interview: Andrew**



**"Inkscape is particularly easy to write plug-ins for"**



**Please introduce yourself; what is your name and where in the world do you live?**

Andrew. Vancouver, Canada



**When did you first meet the Inkscape Community? Please explain how you came to join the project?**

Inkscape is particularly easy to write plug-ins for, so, when I wanted to do something a little out-of-the-ordinary, I drifted to Inkscape to get it done.



**What are the areas and/or teams of the project that you contribute to or have contributed to?**

Plug-ins



**What motivates you to contribute to the Inkscape project? What do you like about the community?**

It is friendly and open. When I wrote one early plug-in, a member used it to create a bit of original art and sent it to me. He promised to send me a large hard-copy as well. It's the thought that counts.



**If you could wave a magic wand, what one thing would you like to see in the Inkscape project?**

1. A new UI for spline drawing. 2. A new UI to the XML. The structure of an SVG drawing can be quite complex. On most occasions, I end up rearranging it in the XML editor.



_________________

How can we contact you for details / clarifications?kurn@sfu.ca