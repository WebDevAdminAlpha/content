

# Inkscape version 1.01 patches crashes & bugs within tools & LPEs 

## Includes experimental Scribus PDF extension 



Inkscape's team of developers has patched bugs and crashes, and generally improve on version 1.0 in this latest release, version 1.01. You'll find a few features, too, along with translation updates for more than a dozen languages. 

Built with the power of a team of mostly volunteers, this open source vector editor represents the work of many hearts and hands from around the world, ensuring that Inkscape remains available free for everyone to download and enjoy.

Special thanks go to the project's Google Summer of Code students (*link to the students?*) whose hard work also made it into this release. 

#### Selectors and CSS dialog back in action!

The **Selectors and CSS dialog** that was [hidden and labelled as 'experimental' in Inkscape 1.0](https://wiki.inkscape.org/wiki/index.php?title=Release_notes/1.0#Selectors_and_CSS_.5BExperimental.2C_hidden.5D) is now available from the **Object** menu in 1.01. This means you'll be able to create color-managed SVGs that can be used with Scribus again.

[![img](https://wiki.inkscape.org/wiki/images/c/c7/Peek_2020-08-24_23-46.gif)](https://wiki.inkscape.org/wiki/index.php?title=File:Peek_2020-08-24_23-46.gif)

*Selectors and CSS dialog usage example*

#### **Experimental Color-managed PDF export using Scribus**

For those of you longing for the day when Inkscape and CMYK are compatible, there's hope on the horizon. Presenting... the **experimental** Scribus PDF export extension! 

It's early days, so there are a [few important steps to follow in order to generate that PDF](https://wiki.inkscape.org/wiki/index.php?title=Release_notes/1.0.1#New_Features). Make sure you double check the result before sending it to the printer. You'll find this feature as one of the many export formats in the 'Save as' and 'Save a Copy' dialogs. Leave your feedback about what works / doesn't work at https://inkscape.org/inbox.

Other colorful features include the following: **Color > List All**, a new extension that lists all colors used in a document, the **Interpolate extension** now works on gradients in fills and strokes and named colors, and the **Render Barcode extension** allows you to define an ID for QR code groups.



#### Crash & Bug Fixes

MacOS users will be happy to learn that the **AppImage** now comes with Python 3.8. Improvements have been made to the **zoom correction factor,** the **document properties dialog** and **Zooming**.

A number of tools are working better as of version 1.01.

Tweaks to the 3D-box, Eraser and Gradient tools will please users along with those to the Node, Pencil, Selector and Text tools.

Certain Live Path Effects (LPEs) are more stable. The **Clone original LPE** that now works with text elements and can better handle transformations, such as moving, stretching and shearing.

[![img](https://wiki.inkscape.org/wiki/images/2/29/Clone_LPE.gif)](https://wiki.inkscape.org/wiki/index.php?title=File:Clone_LPE.gif)

Clone Original LPE usage example



Other improvements include tweaks to the **Knot LPE** , the **LPE selection dialog**, the **Roughen LPE**, the **Fill between many** option, and Inkscape keeps going when selecting an object that is used for the **Pattern-Along-Path LPE**.



**For more details on specific updates in Inkscape 1.01, check out the [Release Notes](https://wiki.inkscape.org/wiki/index.php/Release_notes/1.0.1).**

#### Download Inkscape 1.01 now

**Head [here to download Inkscape 1.01](https://inkscape.org/release/inkscape-1.0/?latest=1)[ for Linux, Windows or macOS] ???).**

Version 1.01 is an updated of the stable version 1.0, released in May 2020, of this free and open-source vector editor.



#### Reach out & join the community

Questions, comments and feedback on Inkscape are welcome. For user support, head to [Inkscape's chat](https://chat.inkscape.org/channel/inkscape_user), where community volunteers field help requests of all sorts. To report bugs, fill out a report directly at the [Inbox](https://gitlab.com/inkscape/inbox/-/issues/).

From programmers and translators to writers, researchers and artists, the Inkscape community is a friendly space to learn and share, build skills and meet others interested in free software and vector editing. Visit Inkscape's [Community Page](https://inkscape.org/community/) for ways to connect.

#### **Draw Freely!**





