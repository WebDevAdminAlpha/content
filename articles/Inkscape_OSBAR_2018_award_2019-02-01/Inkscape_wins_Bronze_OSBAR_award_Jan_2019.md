## Inkscape takes home 2018 Bronze OSBAR award at Univention Summit in Bremen

Inkscape took home the 2018 Bronze Open Source Business Award (OSBAR) this year at the Open Source Business Alliance's (OSB Alliance (https://award.osb-alliance.de/) annual awards event, held in Bremen, Germany, during the Univention Summit (https://www.univention-summit.com/). The OSBARs are considered the "Oscars" of Open Source. 

The project was one of three award winners; OpenProject (https://openproject.org) took home the Silver while HumHub (https://www.humhub.org/en) won the Gold OSBAR.

Inkscape Community member Maren Hachmann was on hand to receive the award, which was sponsored by Kopano (https://kopano.com/).

Five OSBAR jury members reviewed the 13 contenders: Fred Schröder, an IT consultant for Linux and open source business; Anna Brakoniecka, C.A.P.E. IT and founder of Scholar Online; Henrik Hasenkamp, Gridscale; Leena Simon, digital philosopher and IT consultant, and Sabine Kahlenberg, IT Marketing and Communication expert. 

In addition to the award, Inkscaper Maren enjoyed a guided tour of Bremen (https://www.bremen-tourism.de/fairytales-legends) and learned about its history, admired its architecture and delved into its fairytale connections. 

Inkscape (https://inkscape.org/) is a professional-grade vector drawing application. In contrast to raster graphics, vector graphics can be scaled infinitely large without reducing image quality. This makes Inkscape incredibly useful for many graphic design and interface design applications.  Inkscape is used by businesses, professionals, hobbyists, and students. Its source code is available on GitLab and distributed free-of-charge for GNU/Linux, Windows, and MacOS.

The goal of the Inkscape project is embodied in its slogan “Draw Freely”. This is an invitation to individuals and businesses alike. We wish for all people, professionals and amateurs, rich and poor, to have access to excellent creative software. In providing this software free-of-charge, in a manner that respects the liberty of its users, we hope to make the world a more equitably-served, thoughtfully-crafted, and beautifully-designed place.



## SOCIAL MEDIA POSTS 

### TWITTER / MASTODON

We're thrilled to be the 2018 Bronze OSBAR award winner from the Open Source Business Alliance (@osballiance) at #UniventionSummit in Bremen, Germany. Thanks to @kopanobv for sponsoring  our prize! Silver and Gold winners were @openproject (https://twitter.com/openproject) and @HumHub (https://twitter.com/humhub). 

-- images in the issue #35 on GL

Can we like and / or retweet the following? 

https://twitter.com/kopanobv/status/1091009236233007104


### FACEBOOK 

Inkscape is thrilled to be awarded the 2018 Bronze OSBAR by the Open Source Business Alliance (@osballiance) during the #UniventionSummit in Bremen, Germany, on January 31, 2019. 

Thanks to @kopanobv for sponsoring  our prize! The Silver and Gold winners were @openproject (https://openproject.org) and @HumHub (https://www.humhub.org)

#opensource #inkscape 

-- images in the issue #35 - on GL 
