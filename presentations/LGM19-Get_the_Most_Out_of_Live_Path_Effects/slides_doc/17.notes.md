# SLIDE 17
![Goodby](previews/15.png "Goodby")
## SLIDE CONTENT
### Thank you!
Thank you!  
@jabiertxof  
www.inkscape.org  
chat.inkscape.org  

https://tinyurl.com/lpe-lgm19
## SLIDE NOTES
Thanks to LGM, K8 and Inkscape for allowing me to be here. Special thanks to @moini and @michele. They support Inkscape and me in an invaluable way. See you all in France!

Video time total
9:23 = 9 minutes and 23 seconds
...
## SLIDE NOTES PRONOUNCE
...

