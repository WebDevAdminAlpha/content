#!/usr/bin/env python3
from http.server import HTTPServer, BaseHTTPRequestHandler
import os, webbrowser
 
 
class StaticServer(BaseHTTPRequestHandler):
 
    def do_GET(self):
        
        root = os.path.dirname(os.path.abspath(__file__))
        if self.path == '/':
            filename = root + '/index.html'
        else:
            filename = root + self.path.split("?")[0]
        self.send_response(200)
        if filename[-4:] == '.css':
            self.send_header('Content-type', 'text/css')
        elif filename[-5:] == '.json':
            self.send_header('Content-type', 'application/javascript')
        elif filename[-3:] == '.js':
            self.send_header('Content-type', 'application/javascript')
        elif filename[-4:] == '.ico':
            self.send_header('Content-type', 'image/x-icon')
        elif filename[-6:] == '.woff2':
            self.send_header('Content-type', 'font/woff2')
        elif filename[-4:] == '.svg':
            self.send_header('Content-type', 'image/svg+xml')
        else:
            self.send_header('Content-type', 'text/html')
        self.send_header("Access-Control-Allow-Headers", 
                         "Origin, X-Requested-With, Content-Type, Accept")
        self.send_header("Access-Control-Allow-Origin", "*")
        self.end_headers()
        with open(filename, 'rb') as fh:
            html = fh.read()
            #html = bytes(html, 'utf8')
            self.wfile.write(html)
 
def run(server_class=HTTPServer, handler_class=StaticServer, port=8001):
    server_address = ('localhost', port)
    httpd = server_class(server_address, handler_class)
    print('Starting httpd on localhost:{}'.format(port))
    webbrowser.get('firefox').open_new_tab('localhost:8001')
    httpd.serve_forever()
 
run()
